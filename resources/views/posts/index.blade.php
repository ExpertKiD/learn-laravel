@extends('layouts.app')

@section('content')


<h1>Posts</h1>

@if(count($posts) >0)
	@foreach($posts as $post)
		<div class="card card-body">
			<h2><a href="/posts/{{$post->id}}">{{ $post->title }}</a></h2>
			<small>Wriiten on {{$post->created_at}} by {{ $post->user->name }}</small>
		</div>		
	@endforeach
	{{$posts->links()}}
@else

<p>No posts found</p>

@endif
@endsection