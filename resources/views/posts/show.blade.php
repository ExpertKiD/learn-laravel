@extends('layouts.app')

@section('content')
<a href="/posts" class="btn btn-default">Go Back</a>

	<h1>{{$post->title}}</h1>
	

	<div>{!! $post->body !!}</div>
	<hr>
	<small>Written on {{$post->created_at}} by {{ $post->user->name }}</small>
	@auth
		@if(Auth::user()->id == $post->user->id)
			<hr>
			<a class="btn btn-primary" href="/posts/{{$post->id}}/edit">Edit</a>

			{!! Form::open(['action'=>['PostsController@destroy',$post->id],'method'=>'POST','class'=>'float-right'  ]) !!}

				{{ Form::hidden('_method','DELETE') }}
				{{ Form::submit('Delete',['class'=>'btn btn-danger'])}}
			{!! Form::close() !!}
		@endif
	@endauth
@endsection